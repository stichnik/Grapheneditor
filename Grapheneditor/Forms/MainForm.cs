﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
namespace Grapheneditor
{
	public partial class MainForm : Form
	{
		private static int DefaultNameIterator = 0;
		private static int NodeIdIterator = 0;


		private List<Node> nodelist;
		public MainForm()
		{
			InitializeComponent();
			nodelist = new List<Node>();
		}

		private void AddNodeMenuItem_Click_1(object sender, EventArgs e)
		{
			
			string nodename = Interaction.InputBox("Enter name of new node", "Adding node", "node");
			if (nodename != "")
			{
				if (nodename == "node")
				{
					nodename += DefaultNameIterator;
					DefaultNameIterator++;
				}
				//node names need to be unique so that the user knows which node is which in the table based representation
				foreach(Node n in nodelist.ToList())
				{
					if (n.Name == nodename)
					{
						MessageBox.Show("There already is a node with the name "+nodename+". Node names are unique, therefore no node was created."
							, "Error creating node",MessageBoxButtons.OK,MessageBoxIcon.Error);
						return;
					}

				}
				Node newnode = new Node(nodename,NodeIdIterator);
				NodeIdIterator++;
				nodelist.Add(newnode);
				RenderNodeTable();
			}
		}


		public void RenderNodeTable()
		{
			tableView.Items.Clear();
			foreach(Node currnode in nodelist)
			{
				//Create a new ListViewItem and add the name of the node as a subitem
				ListViewItem newitem = new ListViewItem(currnode.Name);


				newitem.SubItems.Add(currnode.ListAllEdges());

				//add the new ListViewItem to the ListView
				tableView.Items.Add(newitem);
			}
			tableView.Update();
		}

		private void AddEdgeMenuItem_Click_1(object sender, EventArgs e)
		{
			if (nodelist.Count < 1)
			{
				MessageBox.Show("You can't make an edge without a node. Make at least one node first.","Not enough nodes",MessageBoxButtons.OK,MessageBoxIcon.Error);
				return;
			}
			AddEdgeForm addEdgeForm = new AddEdgeForm
			{
				Nodelist = this.nodelist.ToList()
			};

			addEdgeForm.ShowDialog();
			if (addEdgeForm.DialogResult == DialogResult.OK) {
				Node source = addEdgeForm.Source;
				Node target = addEdgeForm.Target;
				int cost = addEdgeForm.Cost;
				//check if source and target are actually not null (shouldn't be the case though)
				if (source != null && target != null)
				{
					Edge newedge = new Edge(target, cost);
					source.AddEdge(newedge);
				}
				else
				{
					MessageBox.Show("Something went wrong with creating your edge. " +
						"source or target are not set properly. No edge was created", "Error creating edge", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
			}
			RenderNodeTable();
		}

		private void DeleteNodeMenuItem_Click_1(object sender, EventArgs e)
		{
			DeleteNodeForm deleteNodeForm = new DeleteNodeForm
			{
				Nodelist = this.nodelist.ToList()
			};
			deleteNodeForm.ShowDialog();
			if (deleteNodeForm.DialogResult == DialogResult.OK)
			{
				Node delnode = deleteNodeForm.node;
				if (delnode != null)
				{
					foreach (Node n in nodelist)
						n.RemoveEdge(delnode);
					if (nodelist.Contains(delnode))
						nodelist.Remove(delnode);
				}
			}
			RenderNodeTable();
		}

		private void DeleteEdgeToolStripMenuItem_Click_1(object sender, EventArgs e)
		{
			if (NodeIdIterator == 0)
			{
				MessageBox.Show("You can't delete edges when you dont even have nodes.", "No nodes", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			bool noEdges = true;
			foreach(Node n in nodelist)
			{
				if (n.Edgelist.Count > 0)
				{
					noEdges = false;
				}
			}
			if (noEdges)
			{
				MessageBox.Show("You can't delete edges when you dont have any edges.", "No edges", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			DeleteEdgeForm deleteEdgeForm = new DeleteEdgeForm
			{
				Nodelist = nodelist
			};
			deleteEdgeForm.ShowDialog();
			if (deleteEdgeForm.DialogResult == DialogResult.OK)
			{
				Node srcnode = deleteEdgeForm.Node;
				Edge deledge = deleteEdgeForm.Edge;
				if (srcnode.Edgelist.Contains(deledge))
					srcnode.RemoveEdge(deledge.Target);
			}
			RenderNodeTable();
		}

		private void NewToolStripMenuItem_Click(object sender, EventArgs e)
		{
			//setting nodelist to a new list and rerendering the table should in theory reset everything
			nodelist = new List<Node>();
			RenderNodeTable();
		}

		private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
		{

		}

		private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
		{

		}
	}
}
