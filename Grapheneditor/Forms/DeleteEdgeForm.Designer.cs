﻿namespace Grapheneditor
{
	partial class DeleteEdgeForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.lblNode = new System.Windows.Forms.Label();
			this.lblEdge = new System.Windows.Forms.Label();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.SelectorEdge = new System.Windows.Forms.ComboBox();
			this.edgeBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.SelectorNode = new System.Windows.Forms.ComboBox();
			this.nodeBindingSource = new System.Windows.Forms.BindingSource(this.components);
			((System.ComponentModel.ISupportInitialize)(this.edgeBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nodeBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// lblNode
			// 
			this.lblNode.AutoSize = true;
			this.lblNode.Location = new System.Drawing.Point(12, 9);
			this.lblNode.Name = "lblNode";
			this.lblNode.Size = new System.Drawing.Size(33, 13);
			this.lblNode.TabIndex = 0;
			this.lblNode.Text = "Node";
			// 
			// lblEdge
			// 
			this.lblEdge.AutoSize = true;
			this.lblEdge.Location = new System.Drawing.Point(12, 36);
			this.lblEdge.Name = "lblEdge";
			this.lblEdge.Size = new System.Drawing.Size(32, 13);
			this.lblEdge.TabIndex = 1;
			this.lblEdge.Text = "Edge";
			// 
			// btnOK
			// 
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Location = new System.Drawing.Point(12, 60);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 23);
			this.btnOK.TabIndex = 4;
			this.btnOK.Text = "OK";
			this.btnOK.UseVisualStyleBackColor = true;
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(143, 60);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// SelectorEdge
			// 
			this.SelectorEdge.DataSource = this.edgeBindingSource;
			this.SelectorEdge.FormattingEnabled = true;
			this.SelectorEdge.Location = new System.Drawing.Point(51, 33);
			this.SelectorEdge.Name = "SelectorEdge";
			this.SelectorEdge.Size = new System.Drawing.Size(167, 21);
			this.SelectorEdge.TabIndex = 3;
			// 
			// edgeBindingSource
			// 
			this.edgeBindingSource.DataSource = typeof(Grapheneditor.Edge);
			// 
			// SelectorNode
			// 
			this.SelectorNode.DataSource = this.nodeBindingSource;
			this.SelectorNode.DisplayMember = "Name";
			this.SelectorNode.FormattingEnabled = true;
			this.SelectorNode.Location = new System.Drawing.Point(51, 6);
			this.SelectorNode.Name = "SelectorNode";
			this.SelectorNode.Size = new System.Drawing.Size(167, 21);
			this.SelectorNode.TabIndex = 2;
			this.SelectorNode.ValueMember = "Name";
			this.SelectorNode.SelectedIndexChanged += new System.EventHandler(this.SelectorNode_SelectedIndexChanged);
			// 
			// nodeBindingSource
			// 
			this.nodeBindingSource.DataSource = typeof(Grapheneditor.Node);
			// 
			// DeleteEdgeForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(230, 91);
			this.Controls.Add(this.SelectorNode);
			this.Controls.Add(this.SelectorEdge);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.lblEdge);
			this.Controls.Add(this.lblNode);
			this.Name = "DeleteEdgeForm";
			this.Text = "Delete edge";
			this.Load += new System.EventHandler(this.FormLoaded);
			((System.ComponentModel.ISupportInitialize)(this.edgeBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nodeBindingSource)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblNode;
		private System.Windows.Forms.Label lblEdge;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ComboBox SelectorEdge;
		private System.Windows.Forms.ComboBox SelectorNode;
		private System.Windows.Forms.BindingSource nodeBindingSource;
		private System.Windows.Forms.BindingSource edgeBindingSource;
	}
}