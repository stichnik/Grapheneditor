﻿namespace Grapheneditor
{
	partial class AddEdgeForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.SelectorSource = new System.Windows.Forms.ComboBox();
			this.SourceBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.SelectorTarget = new System.Windows.Forms.ComboBox();
			this.TargetBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.lblSource = new System.Windows.Forms.Label();
			this.lblTarget = new System.Windows.Forms.Label();
			this.lblCost = new System.Windows.Forms.Label();
			this.SelectorCost = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.SourceBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.TargetBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SelectorCost)).BeginInit();
			this.SuspendLayout();
			// 
			// btnOK
			// 
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Location = new System.Drawing.Point(12, 92);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 23);
			this.btnOK.TabIndex = 4;
			this.btnOK.Text = "OK";
			this.btnOK.UseVisualStyleBackColor = true;
			this.btnOK.Click += new System.EventHandler(this.OKbtn_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(140, 92);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// SelectorSource
			// 
			this.SelectorSource.DataSource = this.SourceBindingSource;
			this.SelectorSource.DisplayMember = "Name";
			this.SelectorSource.FormattingEnabled = true;
			this.SelectorSource.Location = new System.Drawing.Point(68, 12);
			this.SelectorSource.Name = "SelectorSource";
			this.SelectorSource.Size = new System.Drawing.Size(147, 21);
			this.SelectorSource.TabIndex = 1;
			this.SelectorSource.ValueMember = "Name";
			this.SelectorSource.SelectedIndexChanged += new System.EventHandler(this.SourceSelector_SelectedIndexChanged);
			// 
			// SourceBindingSource
			// 
			this.SourceBindingSource.DataSource = typeof(Grapheneditor.Node);
			// 
			// SelectorTarget
			// 
			this.SelectorTarget.DataSource = this.TargetBindingSource;
			this.SelectorTarget.DisplayMember = "Name";
			this.SelectorTarget.FormattingEnabled = true;
			this.SelectorTarget.Location = new System.Drawing.Point(68, 39);
			this.SelectorTarget.Name = "SelectorTarget";
			this.SelectorTarget.Size = new System.Drawing.Size(147, 21);
			this.SelectorTarget.TabIndex = 2;
			this.SelectorTarget.ValueMember = "Name";
			this.SelectorTarget.SelectedIndexChanged += new System.EventHandler(this.TargetSelector_SelectedIndexChanged);
			// 
			// TargetBindingSource
			// 
			this.TargetBindingSource.DataSource = typeof(Grapheneditor.Node);
			// 
			// lblSource
			// 
			this.lblSource.AutoSize = true;
			this.lblSource.Location = new System.Drawing.Point(12, 15);
			this.lblSource.Name = "lblSource";
			this.lblSource.Size = new System.Drawing.Size(41, 13);
			this.lblSource.TabIndex = 0;
			this.lblSource.Text = "Source";
			// 
			// lblTarget
			// 
			this.lblTarget.AutoSize = true;
			this.lblTarget.Location = new System.Drawing.Point(12, 42);
			this.lblTarget.Name = "lblTarget";
			this.lblTarget.Size = new System.Drawing.Size(38, 13);
			this.lblTarget.TabIndex = 0;
			this.lblTarget.Text = "Target";
			// 
			// lblCost
			// 
			this.lblCost.AutoSize = true;
			this.lblCost.Location = new System.Drawing.Point(12, 68);
			this.lblCost.Name = "lblCost";
			this.lblCost.Size = new System.Drawing.Size(28, 13);
			this.lblCost.TabIndex = 0;
			this.lblCost.Text = "Cost";
			// 
			// SelectorCost
			// 
			this.SelectorCost.Location = new System.Drawing.Point(68, 66);
			this.SelectorCost.Name = "SelectorCost";
			this.SelectorCost.Size = new System.Drawing.Size(120, 20);
			this.SelectorCost.TabIndex = 3;
			this.SelectorCost.ValueChanged += new System.EventHandler(this.CostSelector_ValueChanged);
			// 
			// AddEdgeForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(224, 122);
			this.Controls.Add(this.SelectorCost);
			this.Controls.Add(this.lblCost);
			this.Controls.Add(this.lblTarget);
			this.Controls.Add(this.lblSource);
			this.Controls.Add(this.SelectorTarget);
			this.Controls.Add(this.SelectorSource);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Name = "AddEdgeForm";
			this.Text = "Add Edge";
			this.Load += new System.EventHandler(this.FormLoaded);
			((System.ComponentModel.ISupportInitialize)(this.SourceBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.TargetBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SelectorCost)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ComboBox SelectorSource;
		private System.Windows.Forms.BindingSource SourceBindingSource;
		private System.Windows.Forms.ComboBox SelectorTarget;
		private System.Windows.Forms.Label lblSource;
		private System.Windows.Forms.Label lblTarget;
		private System.Windows.Forms.Label lblCost;
		private System.Windows.Forms.NumericUpDown SelectorCost;
		private System.Windows.Forms.BindingSource TargetBindingSource;
	}
}