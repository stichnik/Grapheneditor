﻿namespace Grapheneditor
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.menuStrip = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
			this.AddNodeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.DeleteNodeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.AddEdgeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.DeleteEdgeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tableView = new System.Windows.Forms.ListView();
			this.Node = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.Edges = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip
			// 
			this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripItem});
			this.menuStrip.Location = new System.Drawing.Point(0, 0);
			this.menuStrip.Name = "menuStrip";
			this.menuStrip.Size = new System.Drawing.Size(800, 24);
			this.menuStrip.TabIndex = 1;
			this.menuStrip.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// newToolStripMenuItem
			// 
			this.newToolStripMenuItem.Name = "newToolStripMenuItem";
			this.newToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.newToolStripMenuItem.Text = "New";
			this.newToolStripMenuItem.Click += new System.EventHandler(this.NewToolStripMenuItem_Click);
			// 
			// editToolStripItem
			// 
			this.editToolStripItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddNodeMenuItem,
            this.DeleteNodeMenuItem,
            this.AddEdgeMenuItem,
            this.DeleteEdgeToolStripMenuItem});
			this.editToolStripItem.Name = "editToolStripItem";
			this.editToolStripItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripItem.Text = "Edit";
			// 
			// AddNodeMenuItem
			// 
			this.AddNodeMenuItem.Name = "AddNodeMenuItem";
			this.AddNodeMenuItem.Size = new System.Drawing.Size(137, 22);
			this.AddNodeMenuItem.Text = "Add node";
			this.AddNodeMenuItem.Click += new System.EventHandler(this.AddNodeMenuItem_Click_1);
			// 
			// DeleteNodeMenuItem
			// 
			this.DeleteNodeMenuItem.Name = "DeleteNodeMenuItem";
			this.DeleteNodeMenuItem.Size = new System.Drawing.Size(137, 22);
			this.DeleteNodeMenuItem.Text = "Delete node";
			this.DeleteNodeMenuItem.Click += new System.EventHandler(this.DeleteNodeMenuItem_Click_1);
			// 
			// AddEdgeMenuItem
			// 
			this.AddEdgeMenuItem.Name = "AddEdgeMenuItem";
			this.AddEdgeMenuItem.Size = new System.Drawing.Size(137, 22);
			this.AddEdgeMenuItem.Text = "Add edge";
			this.AddEdgeMenuItem.Click += new System.EventHandler(this.AddEdgeMenuItem_Click_1);
			// 
			// DeleteEdgeToolStripMenuItem
			// 
			this.DeleteEdgeToolStripMenuItem.Name = "DeleteEdgeToolStripMenuItem";
			this.DeleteEdgeToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
			this.DeleteEdgeToolStripMenuItem.Text = "Delete edge";
			this.DeleteEdgeToolStripMenuItem.Click += new System.EventHandler(this.DeleteEdgeToolStripMenuItem_Click_1);
			// 
			// tableView
			// 
			this.tableView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Node,
            this.Edges});
			this.tableView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableView.Location = new System.Drawing.Point(0, 24);
			this.tableView.Name = "tableView";
			this.tableView.Size = new System.Drawing.Size(800, 426);
			this.tableView.TabIndex = 2;
			this.tableView.UseCompatibleStateImageBehavior = false;
			this.tableView.View = System.Windows.Forms.View.Details;
			// 
			// Node
			// 
			this.Node.Text = "Node";
			this.Node.Width = 126;
			// 
			// Edges
			// 
			this.Edges.Text = "Edges";
			this.Edges.Width = 525;
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.openToolStripMenuItem.Text = "Open";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.saveToolStripMenuItem.Text = "Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.tableView);
			this.Controls.Add(this.menuStrip);
			this.Name = "MainForm";
			this.Text = "Grapheneditor";
			this.menuStrip.ResumeLayout(false);
			this.menuStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip;
		private System.Windows.Forms.ToolStripMenuItem editToolStripItem;
		private System.Windows.Forms.ToolStripMenuItem AddNodeMenuItem;
		private System.Windows.Forms.ToolStripMenuItem DeleteNodeMenuItem;
		private System.Windows.Forms.ToolStripMenuItem AddEdgeMenuItem;
		private System.Windows.Forms.ToolStripMenuItem DeleteEdgeToolStripMenuItem;
		private System.Windows.Forms.ListView tableView;
		private System.Windows.Forms.ColumnHeader Node;
		private System.Windows.Forms.ColumnHeader Edges;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
	}
}

