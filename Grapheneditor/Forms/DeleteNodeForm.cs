﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grapheneditor
{
	public partial class DeleteNodeForm : Form
	{
		internal Node node;
		internal List<Node> Nodelist { get; set; }

		//this is the constructor, the name is just confusing
		public DeleteNodeForm()
		{
			InitializeComponent();
		}

		private void SelectorNode_SelectedIndexChanged(object sender, EventArgs e)
		{
			node = (Node)SelectorNode.SelectedItem;
		}

		private void FormLoaded(object sender, EventArgs e)
		{
			//load all the nodes in the nodelist into the binding source
			//if a node ever gets deleted, its not a problem, because the binding source gets rebuilt everytime the form gets loaded
			foreach(Node n in Nodelist)
			{
				NodeBindingSource.Add(n);
			}
			//set the default selection so that a NULL pointer exception is impossible
			node = (Node)SelectorNode.SelectedItem;
		}
	}
}
