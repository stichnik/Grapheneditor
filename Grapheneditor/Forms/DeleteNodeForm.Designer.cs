﻿namespace Grapheneditor
{
	partial class DeleteNodeForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.SelectorNode = new System.Windows.Forms.ComboBox();
			this.NodeBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.lblNode = new System.Windows.Forms.Label();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.NodeBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// SelectorNode
			// 
			this.SelectorNode.DataSource = this.NodeBindingSource;
			this.SelectorNode.DisplayMember = "Name";
			this.SelectorNode.FormattingEnabled = true;
			this.SelectorNode.Location = new System.Drawing.Point(53, 6);
			this.SelectorNode.Name = "SelectorNode";
			this.SelectorNode.Size = new System.Drawing.Size(151, 21);
			this.SelectorNode.TabIndex = 1;
			this.SelectorNode.ValueMember = "Name";
			this.SelectorNode.SelectedIndexChanged += new System.EventHandler(this.SelectorNode_SelectedIndexChanged);
			// 
			// NodeBindingSource
			// 
			this.NodeBindingSource.DataSource = typeof(Grapheneditor.Node);
			// 
			// lblNode
			// 
			this.lblNode.AutoSize = true;
			this.lblNode.Location = new System.Drawing.Point(12, 9);
			this.lblNode.Name = "lblNode";
			this.lblNode.Size = new System.Drawing.Size(33, 13);
			this.lblNode.TabIndex = 0;
			this.lblNode.Text = "Node";
			// 
			// btnOK
			// 
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Location = new System.Drawing.Point(12, 33);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 23);
			this.btnOK.TabIndex = 2;
			this.btnOK.Text = "OK";
			this.btnOK.UseVisualStyleBackColor = true;
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(129, 33);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// DeleteNodeForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(217, 68);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.lblNode);
			this.Controls.Add(this.SelectorNode);
			this.Name = "DeleteNodeForm";
			this.Text = "Delete node";
			this.Load += new System.EventHandler(this.FormLoaded);
			((System.ComponentModel.ISupportInitialize)(this.NodeBindingSource)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.ComboBox SelectorNode;
		private System.Windows.Forms.Label lblNode;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.BindingSource NodeBindingSource;
	}
}