﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grapheneditor
{
	public partial class DeleteEdgeForm : Form
	{
		public List<Node> Nodelist { get; internal set; }
		public Node Node { get; private set; }
		public Edge Edge { get; private set; }
		public DeleteEdgeForm()
		{
			InitializeComponent();
		}

		private void FormLoaded(object sender, EventArgs e)
		{
			SelectorEdge.DisplayMember = "TargetString";
			foreach (Node n in Nodelist)
			{
				nodeBindingSource.Add(n);
			}
			Node = (Node)SelectorNode.SelectedItem;
			RebuildEdgeBindingSource(Node);
			Edge = (Edge)SelectorEdge.SelectedItem;
		}
		private void RebuildEdgeBindingSource(Node srcnode)
		{
			if (srcnode == null)
				return;
			edgeBindingSource.Clear();
			foreach (Edge e in srcnode.Edgelist)
			{
				edgeBindingSource.Add(e);
			}
		}

		private void SelectorNode_SelectedIndexChanged(object sender, EventArgs e)
		{
			Node = (Node)SelectorNode.SelectedItem;
			RebuildEdgeBindingSource(Node);
			Edge = (Edge)SelectorEdge.SelectedItem;
		}
	}
}
