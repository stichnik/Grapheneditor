﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grapheneditor
{
	public partial class AddEdgeForm : Form
	{
		internal List<Node> Nodelist { get; set; }
		internal Node Source;
		internal Node Target;
		internal int Cost;
		public AddEdgeForm()
		{
			InitializeComponent();
			SelectorCost.Controls[0].Enabled = false;
			SelectorCost.Controls[0].Visible = false;
		}

		private void OKbtn_Click(object sender, EventArgs e)
		{

		}

	   

		private void FormLoaded(object sender, EventArgs e)
		{
			//load all the nodes in the nodelist into the binding source
			//if a node ever gets deleted, its not a problem, because the binding source gets rebuilt everytime the form gets loaded
			foreach (Node n in Nodelist)
			{
				SourceBindingSource.Add(n);
				TargetBindingSource.Add(n);
			}
			//set the default selection so that a NULL pointer exception is impossible
			Source = (Node)SelectorSource.SelectedItem;
			Target = (Node)SelectorTarget.SelectedItem;
			Cost = (int)SelectorCost.Value;
		}

		private void TargetSelector_SelectedIndexChanged(object sender, EventArgs e)
		{
			Target = (Node)SelectorTarget.SelectedItem;
		}

		private void SourceSelector_SelectedIndexChanged(object sender, EventArgs e)
		{
			Source = (Node)SelectorSource.SelectedItem;
		}

		private void CostSelector_ValueChanged(object sender, EventArgs e)
		{
			Cost = (int)SelectorCost.Value;
		}

		
	}
}
