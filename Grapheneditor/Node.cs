﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grapheneditor
{
	public class Node : IEquatable<Node>
	{
		public List<Edge> Edgelist { get; private set; }
		public string Name { get; set; }
		private int id;
		public Node(string name,int id){
			Name = name;
			Edgelist = new List<Edge>();
			this.id = id;
		}

		


		public void AddEdge(Node target, int cost)
		{
			Edge newedge = new Edge(target,cost);
			Edgelist.Add(newedge);
		}
		public void AddEdge(Edge edge)
		{
			Edgelist.Add(edge);
		}

		public string ListAllEdges()
		{

			string retvalue="";
			if (Edgelist.Count != 0)
			{
				Edge last = Edgelist.Last();

				foreach (Edge e in Edgelist)
				{
					retvalue += e.Target.Name + " - Cost: " + e.Cost.ToString();
					if (!e.Equals(last))
					{
						retvalue += " | ";
					}
				}
			}
			return retvalue;
		}
		public void RemoveEdge(Node tarnode)
		{
			foreach(Edge e in Edgelist.ToList())
			{
				if (e.Target == tarnode)
					Edgelist.Remove(e);
			}
			
		}
		public bool Equals(Node comp)
		{
			if (this.id == comp.id)
				return true;
			return false;
		}
	}
}
