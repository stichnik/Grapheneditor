﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grapheneditor
{
	public class Edge : IEquatable<Edge>
	{
		

		internal int Cost { get; }
		public Node Target { get; set; }
		public string TargetString => Target.Name+" | Cost:"+Cost.ToString();

		//private readonly int cost;
		public Edge(Node target)
		{
			this.Target=target;
			Cost = 0;
		}

		public Edge(Node target, int cost) : this(target)
		{
			this.Cost = cost;
		}

		public bool Equals(Edge other)
		{
			if (this.Target == other.Target && this.Cost == other.Cost)
				return true;
			return false;
		}
	}
}
